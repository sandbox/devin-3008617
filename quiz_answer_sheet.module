<?php

/**
 * @file
 * Code for the Quiz answer sheet module.
 */

/**
 * Implements hook_form_quiz_question_answering_form_alter().
 *
 * Add a link to the answer sheet.
 */
function quiz_answer_sheet_form_quiz_question_answering_form_alter(&$form, $form_state) {
  $quiz_node = node_load(arg(1));
  $result_id = !empty($_SESSION['quiz'][$quiz_node->nid]['result_id']) ? $_SESSION['quiz'][$quiz_node->nid]['result_id'] : NULL;
  $quiz_result = quiz_result_load($result_id);
  $quiz_node = node_load($quiz_result->nid, $quiz_result->vid);
  if ($quiz_node->show_answer_sheet) {
    $answer = l("Answer sheet", "node/$quiz_node->nid/quiz-results/$result_id/by-question", array('attributes' => array('class' => array('answer-sheet-link'))));
    $form['answer_sheet']['#markup'] = $answer;
    $form['answer_sheet']['#weight'] = -1;
  }

  $questions = entity_load('quiz_result_answer', FALSE, array('result_id' => $result_id));
  if (count($questions) >= variable_get('quiz_pager_start', 100)) {
    $answered = 0;
    $total = 0;

    // Get a list of all the node types in bulk, because the question type isn't
    // provided by getLayout(). We don't use to use VID here, since types never
    // change.
    $nids = array();
    foreach ($quiz_result->getLayout() as $idx => $question) {
      $nids[] = $question['nid'];
    }
    $question_node_types = db_select('node', 'n')
      ->fields('n', array('nid', 'vid', 'type'))
      ->condition('nid', $nids)
      ->execute()
      ->fetchAllAssoc('nid');

    foreach ($questions as $questionAnswer) {
      $node = $question_node_types[$questionAnswer->question_nid];
      $quizQuestion = _quiz_question_get_instance($node);
      if ($quizQuestion->isQuestion()) {
        /* @var $questionAnswer QuizResultAnswer */
        if ($questionAnswer->answer_timestamp && !$questionAnswer->is_skipped) {
          $answered++;
        }
        $total++;
      }
    }

    $form['navigation']['quiz_progress']['#markup'] = '<p>' . t('You have answered %answered out of %total questions.', array('%answered' => $answered, '%total' => $total)) . '</p>';
    $form['navigation']['quiz_progress']['#weight'] = 90;
  }
}

/**
 * Implements hook_form_quiz_node_form_alter().
 *
 * Add a checkbox for the answer sheet toggle.
 */
function quiz_answer_sheet_form_quiz_node_form_alter(&$form, $form_state) {
  if (isset($form_state['build_info']['args'][0])) {
    $defaults = $form_state['build_info']['args'][0];
  }
  else {
    $defaults = quiz_get_defaults();
  }
  $form['taking']['show_answer_sheet'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show answer sheet'),
    '#description' => t('Show a link to an answer sheet which obeys the @quiz feedback settings. "Allow skipping" and "Allow jumping" must be enabled to use this feature.', array('@quiz' => _quiz_get_quiz_name())),
    '#default_value' => isset($defaults->show_answer_sheet) ? $defaults->show_answer_sheet : NULL,
  );
  $form['#validate'][] = 'quiz_answer_sheet_node_form_validate';
}

/**
 * Implements hook_form_quiz_admin_node_form_alter().
 *
 * Add a checkbox for the global settings.
 */
function quiz_answer_sheet_form_quiz_admin_node_form_alter(&$form, $form_state) {
  quiz_answer_sheet_form_quiz_node_form_alter($form, $form_state);
}

/**
 * Implements hook_schema_alter().
 *
 * Define schema for show answer sheet configuration.
 */
function quiz_answer_sheet_schema_alter(&$schema) {
  $schema['quiz_node_properties']['fields']['show_answer_sheet'] = array(
    'type' => 'int',
    'size' => 'tiny',
    'not null' => TRUE,
    'default' => 0,
  );
}

/**
 * Implements hook_views_pre_view().
 *
 * Exclude the correct column from display if the user cannot review it at this
 * time.
 */
function quiz_answer_sheet_views_pre_view($view) {
  if ($view->name == 'quiz_result_by_question') {
    $fields = $view->display_handler->get_handlers('field');
    if (arg(2) == 'quiz-results') {
      $quiz_result = entity_load_single('quiz_result', arg(3));
      $quiz_node = node_load($quiz_result->nid, $quiz_result->vid);
      if (!quiz_feedback_can_review('correct', $quiz_result)) {
        $fields['is_correct']->options['exclude'] = 1;
      }
      if (!$quiz_node->mark_doubtful) {
        $fields['is_doubtful']->options['exclude'] = 1;
      }
    }
  }
}

/**
 * Validation for quiz node form.
 */
function quiz_answer_sheet_node_form_validate($form, &$form_state) {
  // If the answer sheet has been enabled
  // Ensure that "Allow jumping" and "Allow skipping" are also enabled
  if (!empty($form_state['input']['show_answer_sheet']) && (empty($form_state['input']['allow_skipping']) || empty($form_state['input']['allow_jumping']))) {
    form_set_error('show_answer_sheet', t('Allow skipping and allow jumping must be enabled to use the answer sheet feature.'));
  }
}

/**
 * Implements hook_views_api().
 */
function quiz_answer_sheet_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
