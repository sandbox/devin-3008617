<?php
/**
 * @file
 * quiz_answer_sheet.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function quiz_answer_sheet_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'quiz_result_by_question';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'quiz_node_results_answers';
  $view->human_name = 'Quiz result by question';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Quiz result by question';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['disable_sql_rewrite'] = TRUE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'footable';
  $handler->display->display_options['style_options']['columns'] = array(
    'display_number' => 'display_number',
    'body' => 'body',
    'answer_timestamp' => 'answer_timestamp',
    'is_skipped' => 'is_skipped',
    'is_correct' => 'is_correct',
    'is_doubtful' => 'is_doubtful',
  );
  $handler->display->display_options['style_options']['default'] = 'display_number';
  $handler->display->display_options['style_options']['info'] = array(
    'display_number' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'body' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'answer_timestamp' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'is_skipped' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'is_correct' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'is_doubtful' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['sticky'] = TRUE;
  $handler->display->display_options['style_options']['footable'] = array(
    'expand_all' => FALSE,
    'expand_first' => '0',
    'show_header' => '1',
    'toggle_column' => 'first',
    'icon_expanded' => 'minus',
    'icon_collapsed' => 'plus',
    'bootstrap' => array(
      'striped' => FALSE,
      'bordered' => FALSE,
      'hover' => FALSE,
      'condensed' => FALSE,
    ),
    'component' => array(
      'paging' => array(
        'enabled' => 0,
        'count_format' => '{CP} of {TP}',
        'current' => '1',
        'limit' => '5',
        'position' => 'right',
        'size' => '10',
        'countformat' => '{CP} of {TP}',
      ),
      'filtering' => array(
        'enabled' => 0,
        'delay' => '1200',
        'ignore_case' => TRUE,
        'min' => '3',
        'placeholder' => 'Search',
        'position' => 'right',
        'space' => 'AND',
      ),
      'sorting' => array(
        'enabled' => 0,
      ),
    ),
    'overwrite' => array(
      'xs' => '',
      'phone' => '',
      'sm' => '',
      'md' => '',
      'tablet' => '',
      'lg' => '',
    ),
    'breakpoint' => array(
      'display_number' => array(
        'xs' => 0,
        'phone' => 0,
        'all' => 0,
        'default' => 0,
        'sm' => 0,
        'md' => 0,
        'tablet' => 0,
        'lg' => 0,
      ),
      'body' => array(
        'xs' => 'xs',
        'phone' => 'phone',
        'sm' => 0,
        'all' => 0,
        'default' => 0,
        'md' => 0,
        'tablet' => 0,
        'lg' => 0,
      ),
      'answer_timestamp' => array(
        'xs' => 'xs',
        'phone' => 'phone',
        'sm' => 0,
        'all' => 0,
        'default' => 0,
        'md' => 0,
        'tablet' => 0,
        'lg' => 0,
      ),
      'is_skipped' => array(
        'xs' => 'xs',
        'phone' => 'phone',
        'sm' => 0,
        'all' => 0,
        'default' => 0,
        'md' => 0,
        'tablet' => 0,
        'lg' => 0,
      ),
      'is_correct' => array(
        'xs' => 'xs',
        'phone' => 'phone',
        'sm' => 0,
        'all' => 0,
        'default' => 0,
        'md' => 0,
        'tablet' => 0,
        'lg' => 0,
      ),
      'is_doubtful' => array(
        'xs' => 'xs',
        'phone' => 'phone',
        'sm' => 0,
        'all' => 0,
        'default' => 0,
        'md' => 0,
        'tablet' => 0,
        'lg' => 0,
      ),
    ),
  );
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['content'] = '<a href="/node/!1/take" class="quiz-back-link">Back to quiz</a>';
  $handler->display->display_options['header']['area']['format'] = 'filtered_html';
  $handler->display->display_options['header']['area']['tokenize'] = TRUE;
  /* Footer: Global: Text area */
  $handler->display->display_options['footer']['area']['id'] = 'area';
  $handler->display->display_options['footer']['area']['table'] = 'views';
  $handler->display->display_options['footer']['area']['field'] = 'area';
  $handler->display->display_options['footer']['area']['content'] = '<a href="/node/!1/take" class="quiz-back-link">Restart quiz</a>';
  $handler->display->display_options['footer']['area']['format'] = 'filtered_html';
  $handler->display->display_options['footer']['area']['tokenize'] = TRUE;
  /* Relationship: Quiz result answer: Question_nid */
  $handler->display->display_options['relationships']['question_nid']['id'] = 'question_nid';
  $handler->display->display_options['relationships']['question_nid']['table'] = 'quiz_node_results_answers';
  $handler->display->display_options['relationships']['question_nid']['field'] = 'question_nid';
  /* Relationship: Quiz result answer: Result_id */
  $handler->display->display_options['relationships']['result_id']['id'] = 'result_id';
  $handler->display->display_options['relationships']['result_id']['table'] = 'quiz_node_results_answers';
  $handler->display->display_options['relationships']['result_id']['field'] = 'result_id';
  /* Field: Quiz result answer: Display_number */
  $handler->display->display_options['fields']['display_number']['id'] = 'display_number';
  $handler->display->display_options['fields']['display_number']['table'] = 'quiz_node_results_answers';
  $handler->display->display_options['fields']['display_number']['field'] = 'display_number';
  $handler->display->display_options['fields']['display_number']['label'] = 'Question number';
  $handler->display->display_options['fields']['display_number']['separator'] = '';
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['relationship'] = 'question_nid';
  $handler->display->display_options['fields']['body']['label'] = 'Question text';
  $handler->display->display_options['fields']['body']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['path'] = 'node/!1/take/[display_number]';
  $handler->display->display_options['fields']['body']['alter']['max_length'] = '50';
  $handler->display->display_options['fields']['body']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['trim'] = TRUE;
  /* Field: Quiz result answer: Answer_timestamp */
  $handler->display->display_options['fields']['answer_timestamp']['id'] = 'answer_timestamp';
  $handler->display->display_options['fields']['answer_timestamp']['table'] = 'quiz_node_results_answers';
  $handler->display->display_options['fields']['answer_timestamp']['field'] = 'answer_timestamp';
  $handler->display->display_options['fields']['answer_timestamp']['label'] = 'Answered';
  $handler->display->display_options['fields']['answer_timestamp']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['answer_timestamp']['alter']['text'] = 'Yes';
  $handler->display->display_options['fields']['answer_timestamp']['empty'] = 'No';
  $handler->display->display_options['fields']['answer_timestamp']['date_format'] = 'long';
  $handler->display->display_options['fields']['answer_timestamp']['second_date_format'] = 'long';
  /* Field: Quiz result answer: Is_skipped */
  $handler->display->display_options['fields']['is_skipped']['id'] = 'is_skipped';
  $handler->display->display_options['fields']['is_skipped']['table'] = 'quiz_node_results_answers';
  $handler->display->display_options['fields']['is_skipped']['field'] = 'is_skipped';
  $handler->display->display_options['fields']['is_skipped']['label'] = 'Skipped';
  $handler->display->display_options['fields']['is_skipped']['not'] = 0;
  /* Field: Quiz result answer: Is_correct */
  $handler->display->display_options['fields']['is_correct']['id'] = 'is_correct';
  $handler->display->display_options['fields']['is_correct']['table'] = 'quiz_node_results_answers';
  $handler->display->display_options['fields']['is_correct']['field'] = 'is_correct';
  $handler->display->display_options['fields']['is_correct']['label'] = 'Correct';
  $handler->display->display_options['fields']['is_correct']['not'] = 0;
  /* Field: Quiz result answer: Is_doubtful */
  $handler->display->display_options['fields']['is_doubtful']['id'] = 'is_doubtful';
  $handler->display->display_options['fields']['is_doubtful']['table'] = 'quiz_node_results_answers';
  $handler->display->display_options['fields']['is_doubtful']['field'] = 'is_doubtful';
  $handler->display->display_options['fields']['is_doubtful']['label'] = 'Marked doubtful';
  $handler->display->display_options['fields']['is_doubtful']['type'] = 'custom';
  $handler->display->display_options['fields']['is_doubtful']['type_custom_true'] = 'X';
  $handler->display->display_options['fields']['is_doubtful']['not'] = 0;
  /* Contextual filter: Global: Null */
  $handler->display->display_options['arguments']['null']['id'] = 'null';
  $handler->display->display_options['arguments']['null']['table'] = 'views';
  $handler->display->display_options['arguments']['null']['field'] = 'null';
  $handler->display->display_options['arguments']['null']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['null']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['null']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['null']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['null']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['null']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['null']['validate']['type'] = 'node';
  $handler->display->display_options['arguments']['null']['validate_options']['types'] = array(
    'quiz' => 'quiz',
  );
  $handler->display->display_options['arguments']['null']['validate_options']['access'] = TRUE;
  /* Contextual filter: Quiz result answer: Result_id */
  $handler->display->display_options['arguments']['result_id']['id'] = 'result_id';
  $handler->display->display_options['arguments']['result_id']['table'] = 'quiz_node_results_answers';
  $handler->display->display_options['arguments']['result_id']['field'] = 'result_id';
  $handler->display->display_options['arguments']['result_id']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['result_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['result_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['result_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['result_id']['summary_options']['items_per_page'] = '25';
  /* Contextual filter: Quiz result: Uid */
  $handler->display->display_options['arguments']['uid']['id'] = 'uid';
  $handler->display->display_options['arguments']['uid']['table'] = 'quiz_node_results';
  $handler->display->display_options['arguments']['uid']['field'] = 'uid';
  $handler->display->display_options['arguments']['uid']['relationship'] = 'result_id';
  $handler->display->display_options['arguments']['uid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['uid']['default_argument_type'] = 'current_user';
  $handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['uid']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['uid']['validate']['type'] = 'user_or_permission';
  $handler->display->display_options['arguments']['uid']['validate_options']['perm'] = 'view any quiz results';
  $handler->display->display_options['arguments']['uid']['validate']['fail'] = 'access denied';
  /* Filter criterion: Content: Question type */
  $handler->display->display_options['filters']['quiz_question_type']['id'] = 'quiz_question_type';
  $handler->display->display_options['filters']['quiz_question_type']['table'] = 'node';
  $handler->display->display_options['filters']['quiz_question_type']['field'] = 'quiz_question_type';
  $handler->display->display_options['filters']['quiz_question_type']['relationship'] = 'question_nid';
  $handler->display->display_options['filters']['quiz_question_type']['operator'] = 'not in';
  $handler->display->display_options['filters']['quiz_question_type']['value'] = array(
    'quiz_directions' => 'quiz_directions',
    'quiz_page' => 'quiz_page',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'node/%/quiz-results/%/by-question';
  $handler->display->display_options['menu']['title'] = 'Answer sheet';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Quiz result by question All';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['path'] = 'node/%/quiz-results/%/by-question/all';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Answer sheet';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $export['quiz_result_by_question'] = $view;

  return $export;
}
